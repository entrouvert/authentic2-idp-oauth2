Install
=======

You just have to install the package in your virtualenv and relaunch, it will
be automatically loaded by the plugin framework.


Settings
========

AUTOMATIC_GRANT:

    A list of URL prefix which are automatically granted scopes without asking
    the user. Example::

        A2_OAUTH2_AUTOMATIC_GRANT = (
                ('http://localhost:8000/', ('read',)),
        )

Web Service proxy
=================

You can configure simple REST web-service in
/admin/authentic2_idp_oauth2/webservice/. URL field can contain template
variable like that:

    http://example.com/info/?user={{ user.username|urlencode }} 

or like:

    http://example.com/categories/?format=json&NameID={{ federations.service_1.links.0|urlencode }}

Supported authentication mechanisms on the target web-service are HMAC-SHA-256
and HMAC-SHA-1 as specified on http://doc.entrouvert.org/portail-citoyen/dev/.

You can access your newly proxy web-service through those URLs:

    http://your-idp.com/idp/oauth2/ws-proxy/<web-service.id>/

or:

    http://your-idp.com/idp/oauth2/ws-proxy/<web-service.slug>/
