from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django import forms

from authentic2.decorators import to_iter
from authentic2.attributes_ng.engine import get_attribute_names
from provider.oauth2.admin import ClientAdmin

from . import models

class WebServiceAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

class AttributeReleaselineForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AttributeReleaselineForm, self).__init__(*args, **kwargs)
        choices = self.choices({'user': None, 'request': None})
        self.fields['attribute_name'].choices = choices
        self.fields['attribute_name'].widget = forms.Select(choices=choices)

    @to_iter
    def choices(self, ctx):
        return [('', _('None'))] + get_attribute_names(ctx)

    class Meta:
        model = models.AttributeRelease

class AttributeReleaseInline(admin.TabularInline):
    model = models.AttributeRelease
    form = AttributeReleaselineForm


class A2ClientAdmin(ClientAdmin):
    inlines = [AttributeReleaseInline]
    fieldsets = (
            (None, {'fields': (
                'name',
                'user',
                'authorized_scopes',
                ) }),
            (_('Location'), {'fields': (
                'url',
                'redirect_uri',
                ) }),
            (_('Credentials'), {'fields': (
                'client_id',
                'client_type',
                'client_secret',
                ) }),
            (_('Logout'),
                {'fields': ('logout_url', 'logout_use_iframe', 'logout_use_iframe_timeout'),}),)

admin.site.register(models.WebService, WebServiceAdmin)
admin.site.register(models.A2Client, A2ClientAdmin)
