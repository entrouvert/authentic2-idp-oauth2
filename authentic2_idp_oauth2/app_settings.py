class AppSettings(object):
    def __init__(self, prefix):
        self.prefix = prefix

    @property
    def AUTOMATIC_GRANT(self):
        return self._setting('AUTOMATIC_GRANT', ())

    def _setting(self, name, dflt):
        from django.conf import settings
        return getattr(settings, self.prefix+name, dflt)


# Ugly? Guido recommends this himself ...
# http://mail.python.org/pipermail/python-ideas/2012-May/014969.html
import sys
app_settings = AppSettings('A2_OAUTH2_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
