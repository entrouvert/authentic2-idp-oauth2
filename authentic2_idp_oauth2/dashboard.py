from django.utils.translation import ugettext_lazy as _

from admin_tools.dashboard import modules


def get_admin_modules():
    '''Show Client model in authentic2 admin'''
    model_list = modules.ModelList(_('OAuth2'),
            models=('authentic2_idp_oauth2.models.A2Client',
                    'authentic2_idp_oauth2.models.WebService',
                ))
    return (model_list,)

