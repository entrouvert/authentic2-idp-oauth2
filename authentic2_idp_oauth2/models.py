from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template import Template

from provider.oauth2.models import Client

from authentic2.models import LogoutUrlAbstract
from authentic2.managers import GetBySlugManager



class A2Client(LogoutUrlAbstract, Client):

    authorized_scopes = models.CharField('automatically granted scopes',
                                         max_length=256, blank=True,
                                         help_text=_('space separated scopes'),
                                         validators = [RegexValidator(('^[a-z]+([ \+][a-z]+)+$'))]
    )
    class Meta:
        verbose_name = _('client')
        verbose_name_plural = _('clients')

    def clean(self):
        self.authorized_scopes = self.authorized_scopes.strip()


class AttributeRelease(models.Model):
    client = models.ForeignKey(A2Client, verbose_name=_('client'))
    name = models.CharField(verbose_name=_('name'), max_length=64)
    attribute_name = models.CharField(max_length=64,
            verbose_name=_('attribute name'))

    class Meta:
        verbose_name = _('OAuth2 attribute release')
        verbose_name_plural = _('oauth2 attribute releases')


class WebService(models.Model):
    AUTH_MECH = (
            ('', 'None'),
            ('hmac-sha256', 'HMAC-SHA-256'),
            ('hmac-sha1', 'HMAC-SHA-1'),
    )

    name = models.CharField(max_length=32)
    slug = models.SlugField(max_length=32)

    url = models.CharField(max_length=1024)

    auth_mech = models.CharField(verbose_name=_('Authentication mechanism'),
            max_length=16, choices=AUTH_MECH, default='', blank=True)
    signature_key = models.CharField(verbose_name=_('Signature key'),
            max_length=128, default='', blank=True)
    verify_certificate = models.BooleanField(verbose_name=_('verify '
        'certificate'), default=True, blank=True)
    allow_redirects = models.BooleanField(verbose_name=_('allows HTTP redirections'),
            help_text=_('it can improve latencies to forbid redirection follow'),
            default=True)
    timeout = models.IntegerField(verbose_name=_('timeout'),
            default=10,
            help_text=_('time in second to wait before '
                'failing to download a datasource'))

    objects = GetBySlugManager()

    def clean(self):
        if self.signature_key and (not self.auth_mech or not self.auth_mech.startswith('hmac-')):
            raise ValidationError(_('You must choose a hashing algorithm if '
                    'you set a signature key'))

    def natural_key(self):
        return (self.slug,)

    def get_url(self, ctx=None):
        if ctx is None:
            ctx = {}
        return Template(self.url).render(ctx)

    def __unicode__(self):
        return self.name
