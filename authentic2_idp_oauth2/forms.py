from django.forms import Form

from provider.oauth2.models import Grant

class EmptyForm(Form):
    def __init__(self, *args, **kwargs):
        self.scope = kwargs.pop('scope')
        super(EmptyForm, self).__init__(*args, **kwargs)

    def save(self, **kwargs):
        return Grant(scope=self.scope)
