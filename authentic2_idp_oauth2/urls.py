from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required

from .views import Authorize

urlpatterns = patterns('authentic2_idp_oauth2.views',
        url('^idp/oauth2/authorize/confirm/?$',
            login_required(Authorize.as_view()), name='authorize'),
        url('^idp/oauth2/', include('provider.oauth2.urls', namespace='oauth2')),
        url('^idp/oauth2/user-info/$', 'user_info', name='a2-idp-oauth2-user-info'),
        url('^idp/oauth2/ws-proxy/(?P<ws_id>.*)/$', 'ws_proxy', name='a2-idp-oauth2-ws-proxy'),
)
